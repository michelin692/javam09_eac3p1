package ioc.dam.m9.uf3.eac3.b3.encripta;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

import javax.crypto.spec.SecretKeySpec;

public class Encriptacio {

    private Cipher cipher;

    public PublicKey getPublica(String fitxer, String algorisme) throws Exception {
        byte[] bytesClau = Files.readAllBytes(new File(fitxer).toPath());
        X509EncodedKeySpec spec = new X509EncodedKeySpec(bytesClau);
        KeyFactory kf = KeyFactory.getInstance(algorisme);
        return kf.generatePublic(spec);
    }

    public SecretKeySpec getClauSecreta(String fitxer, String algorisme) throws IOException {
        byte[] bytesClau = Files.readAllBytes(new File(fitxer).toPath());
        return new SecretKeySpec(bytesClau, algorisme);
    }

    //----------------------------------------------------------------
    
    public void encriptaDades(File original, File encriptat, SecretKeySpec clauSecreta, String algorismeXifrat) throws IOException, GeneralSecurityException {
        
        //IMPLEMENTAR
        byte[] encryptedData = null;    
        cipher = Cipher.getInstance(algorismeXifrat);
        cipher.init(Cipher.ENCRYPT_MODE, clauSecreta);
        encryptedData =  cipher.doFinal(fitxerEnBytes(original));
        escriuAFitxer(encriptat,encryptedData);
        
    }

    //--------------------------------------------------------------
    public void encriptaClau(PublicKey clau, File fitxerClauOriginal, File fitxerClauEncriptada, String algorismeXifrat) throws IOException, GeneralSecurityException {
       
        //IMPLEMENTAR, tenemos que con la clave simetrica y publica de nuria encriptarla y meterla en un fichero.
        byte[] encryptedData = null;
        cipher = Cipher.getInstance(algorismeXifrat);
        cipher.init(Cipher.ENCRYPT_MODE, clau);
        encryptedData =  cipher.doFinal(fitxerEnBytes(fitxerClauOriginal));
        escriuAFitxer(fitxerClauEncriptada,encryptedData);
    }

    
      
    
    public static void main(String[] args) throws IOException, Exception {
        Encriptacio iniEnc = new Encriptacio();

        //System.out.println("Clau Secreta : " + iniEnc.getClauSecreta("UnaClaU/clauSecreta", "AES"));
        //System.out.println("Clau publica Meritxell: " + iniEnc.getPublica("ParellClaus/publica_Meritxell", "RSA"));
        //System.out.println("Clau publica Nuria: " + iniEnc.getPublica("ParellClaus/publica_Nuria", "RSA"));
        File file_clauSecreta = new File("UnaClaU/clauSecreta");
        File file_SortidaClauSecreta = new File("FitxersEncriptats/clauSecreta");
        File file_cartaOriginal = new File("carta.txt");
        File file_cartaEncriptada = new File("FitxersEncriptats/FitxerEncriptat");
        
        //Encripta la clau
        iniEnc.encriptaClau(iniEnc.getPublica("ParellClaus/publica_Nuria", "RSA"),file_clauSecreta,file_SortidaClauSecreta,"RSA");

        //Encripta la carta
        iniEnc.encriptaDades(file_cartaOriginal, file_cartaEncriptada, iniEnc.getClauSecreta("UnaClaU/clauSecreta", "AES"), "AES");
        
    }

          // Mètodes auxiliars 
    private void escriuAFitxer(File out, byte[] aEscriure) throws  IOException {
        out.getParentFile().mkdirs();
        try (FileOutputStream fos = new FileOutputStream(out)) {
            fos.write(aEscriure);
            fos.flush();
        }
        System.out.println("El fitxer ha estat guardat a:  " + out.getPath());
    }

    private byte[] fitxerEnBytes(File f) throws IOException {
        byte[] fbytes;
        try (FileInputStream fis = new FileInputStream(f)) {
            fbytes = new byte[(int) f.length()];
            fis.read(fbytes);
        }
        return fbytes;
    }

}
