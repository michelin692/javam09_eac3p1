package ioc.dam.m9.uf3.eac3.b3.desencripta;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.crypto.Cipher;

import javax.crypto.spec.SecretKeySpec;

public class Desencriptacio {
    private Cipher cipher;
	
	public PrivateKey getPrivada(String fitxer ,String algorisme) throws Exception {
		byte[] bytesClau = Files.readAllBytes(new File(fitxer).toPath());
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(bytesClau);
		KeyFactory kf = KeyFactory.getInstance(algorisme);
		return kf.generatePrivate(spec);
	}

	
	public SecretKeySpec getClauSecreta(String fitxer, String algorisme) throws IOException{
		byte[] bytesClau = Files.readAllBytes(new File(fitxer).toPath());
		return new SecretKeySpec(bytesClau, algorisme);
	}
	
        //-----------------------------------------------------------------
        public void desencriptaClau(PrivateKey clauPrivada, File clauEncriptadaRebuda, File fitxerClauDesencriptat, String algorisme) throws IOException, GeneralSecurityException {
		
            //IMPLEMENTAR
            byte[] encryptedData = null;
            cipher = Cipher.getInstance(algorisme);
            cipher.init(Cipher.DECRYPT_MODE, clauPrivada);
            encryptedData =  cipher.doFinal(fitxerEnBytes(clauEncriptadaRebuda));
            escriuAFitxer(fitxerClauDesencriptat,encryptedData);
            
       }
	

        //----------------------------------------------------------------------------
        public void desencriptaDades(File fitxerEncriptatRebut, File fitxerDesencriptat, SecretKeySpec clauSecreta, String algorisme) throws IOException, GeneralSecurityException {
		
            //IMPLEMENTAR
            byte[] encryptedData = null;    
            cipher = Cipher.getInstance(algorisme);
            cipher.init(Cipher.DECRYPT_MODE, clauSecreta);
            encryptedData =  cipher.doFinal(fitxerEnBytes(fitxerEncriptatRebut));
            escriuAFitxer(fitxerDesencriptat,encryptedData);
	}
	
      
        
   
        
        
	public static void main(String[] args) throws IOException, Exception{
		Desencriptacio iniDes = new Desencriptacio();
		
		//IMPLEMENTAR
                File file_ClauSecretaRebuda = new File("FitxersEncriptats/clauSecreta");
                File file_cartaEncriptadaRebuda = new File("FitxersEncriptats/FitxerEncriptat");
                File file_ClauSecreteDesenciptada = new File("FitxersDesencriptats/clauSecreta");
                File file_cartaDesenciptada = new File("FitxersDesencriptats/FitxerDesencriptat");
                
                iniDes.desencriptaClau(iniDes.getPrivada("ParellClaus/privada_Nuria", "RSA"), file_ClauSecretaRebuda, file_ClauSecreteDesenciptada, "RSA");
                
                iniDes.desencriptaDades(file_cartaEncriptadaRebuda, file_cartaDesenciptada, iniDes.getClauSecreta("FitxersDesencriptats/clauSecreta", "AES"), "AES");
	}
        
        
               
          // Mètodes auxiliars 
        private void escriuAFitxer(File out, byte[] aEscriure) throws IOException{
		out.getParentFile().mkdirs();
            try (FileOutputStream fos = new FileOutputStream(out)) {
                fos.write(aEscriure);
                fos.flush();
            }
		System.out.println("El fitxer ha estat guardat a:  " + out.getPath());
	}
	
	private byte[] fitxerEnBytes(File f) throws IOException{
            byte[] fbytes;
        try (FileInputStream fis = new FileInputStream(f)) {
            fbytes = new byte[(int) f.length()];
            fis.read(fbytes);
        }
	    return fbytes;
	}
        
}
