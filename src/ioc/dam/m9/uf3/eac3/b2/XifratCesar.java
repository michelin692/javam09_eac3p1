/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.dam.m9.uf3.eac3.b2;

/**
 *
 * @author Usuari
 */
import java.io.IOException;
import java.util.Scanner;

public class XifratCesar {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String text;
        int codi;
        char opcio;
        //Introduir un text
        do {
            System.out.print("Introdueix un text per utilitzar-lo: ");
            text = sc.nextLine();
        } while (text.isEmpty());
        //Tot seguit introduirem el desplaçament que utilizarem en el xifrat
         //IMPLEMENTAR
         System.out.print("Introdueix el desplaçament: ");
         codi = sc.nextInt();
        //Introduir l'operació que voldrem fer, aquesta pot ser xifrar o desxifrar
         //IMPLEMENTAR
         System.out.print("Apreta X per xifrar i D per desxifrar:");
         opcio = sc.next().charAt(0);
         
         //System.out.println("texto : " + text+" --- Codigo :" + codi + " --- opcion: " +  opcio);
         if( opcio == "x".charAt(0) || opcio == "X".charAt(0) ){
             System.out.println("Text xifrat: " + xifratCesar(text,codi));
         }else if( opcio == "d".charAt(0) || opcio == "D".charAt(0) ){
             System.out.println("Text desxifrat: " + desxifratCesar(text,codi));
            //System.out.println("por llamar funcion para descifrar");
         }else{
             System.out.println("No has apretat ni X ni D");
         }
    }

    //mètode para xifrar el text
    public static String xifratCesar(String text, int codi) {
        StringBuilder xifrat = new StringBuilder();
        codi = codi % 26;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) >= 'a' && text.charAt(i) <= 'z') {
                if ((text.charAt(i) + codi) > 'z') {
                    xifrat.append((char) (text.charAt(i) + codi - 26));
                } else {
                    xifrat.append((char) (text.charAt(i) + codi));
                }
            } else if (text.charAt(i) >= 'A' && text.charAt(i) <= 'Z') {
                if ((text.charAt(i) + codi) > 'Z') {
                    xifrat.append((char) (text.charAt(i) + codi - 26));
                } else {
                    xifrat.append((char) (text.charAt(i) + codi));
                }
            }
        }
        return xifrat.toString();
    }

    //mètode per desxifrar el text
    public static String desxifratCesar(String text, int codi) {
        StringBuilder xifrat = new StringBuilder();
        codi = codi % 26;
        
        //IMPLEMENTAR
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) >= 'a' && text.charAt(i) <= 'z') {
                if ((text.charAt(i) + codi) > 'z') {
                    xifrat.append((char) (text.charAt(i) + codi - 26));
                } else {
                    xifrat.append((char) (text.charAt(i) - codi));
                }
            } else if (text.charAt(i) >= 'A' && text.charAt(i) <= 'Z') {
                if ((text.charAt(i) + codi) > 'Z') {
                    xifrat.append((char) (text.charAt(i) + codi - 26));
                } else {
                    xifrat.append((char) (text.charAt(i) - codi));
                }
            }
        }
        
        return xifrat.toString();
    }
} //Fi xifrat Cesar